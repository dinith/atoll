<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_details".
 *
 * @property int $id
 * @property string $full_name
 * @property string $given_name
 * @property string $nic
 * @property string $emp_id
 * @property string $emp_st_date
 * @property string $birthday
 * @property string $gender
 * @property string $address
 * @property string $email
 * @property string|null $tel_no
 * @property string $mob_no
 * @property string|null $photo
 * @property int $role_id
 * @property string $emp_type
 * @property string $acc_from
 * @property string|null $acc_to
 *
 * @property UserRole $role
 */
class UserDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'given_name', 'nic', 'emp_id', 'emp_st_date', 'birthday', 'gender', 'address', 'email', 'mob_no', 'role_id', 'emp_type', 'acc_from'], 'required'],
            [['emp_st_date', 'birthday', 'acc_from', 'acc_to'], 'safe'],
            [['role_id'], 'integer'],
            [['key'], 'string'],
            [['full_name', 'address', 'photo'], 'string', 'max' => 255],
            [['given_name', 'email'], 'string', 'max' => 50],
            [['nic'], 'string', 'max' => 20],
            [['emp_id', 'gender', 'emp_type', 'status'], 'string', 'max' => 10],
            [['tel_no', 'mob_no'], 'string', 'max' => 15],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'given_name' => 'Given Name',
            'nic' => 'NIC',
            'emp_id' => 'Employee ID',
            'emp_st_date' => 'Emp St Date',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'address' => 'Address',
            'email' => 'Email',
            'tel_no' => 'Tel. No',
            'mob_no' => 'Mobile No',
            'photo' => 'Photo',
            'role_id' => 'Role ID',
            'emp_type' => 'Employee Type',
            'acc_from' => 'Acc From',
            'acc_to' => 'Acc To',
            'status' => 'Status',
            'key' => 'Key',
        ];
    }

    /**
     * Gets query for [[Role]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'role_id']);
    }
}
