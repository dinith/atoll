<?php

use yii\db\Migration;

/**
 * Class m210209_072118_create_user_details
 */
class m210209_072118_create_user_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('user_details', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(255)->notNull(),
            'given_name' => $this->string(50)->notNull(),
            'nic' => $this->string(7)->notNull(),
            'emp_id' => $this->string(10)->notNull(),
            'emp_st_date' => $this->dateTime()->notNull(),
            'birthday' => $this->dateTime()->notNull(),
            'gender' => $this->dateTime()->notNull(),
            'address' => $this->string(255)->notNull(),
            'email' => $this->string(50)->notNull(),
            'tel_no' => $this->string(15),
            'mob_no' => $this->string(15)->notNull(),
            'photo' => $this->string(255),
            'role_id' => $this->integer()->notNull(),
            'emp_type' => $this->string(10)->notNull(),
            'acc_from' => $this->dateTime()->notNull(),
            'acc_to' => $this->dateTime(),
        ]);

        // $this->addForeignKey(
        //     'fk-user_-role_id',
        //     'user_details',
        //     'role_id',
        //     'user_role',
        //     'id',
        //     'CASCADE'
        // );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('user_details');

        $this->dropForeignKey(
            'fk-user_-role_id',
            'user_details'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_072118_create_user_details cannot be reverted.\n";

        return false;
    }
    */
}
