<?php
use yii\helpers\Html;
?>
<div class="row justify-content-center">
    <div class="col-4">
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Configure Password</p>

                <?php $form = \yii\bootstrap4\ActiveForm::begin(['id' => 'login-form']) ?>

                <?= $form->field($model, 'password', [
                    'options' => ['class' => 'form-group has-feedback'],
                ])->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

                <?= $form->field($model, 'password_repeat', [
                    'options' => ['class' => 'form-group has-feedback'],
                ])->passwordInput(['placeholder' => $model->getAttributeLabel('password_repeat')]) ?>

                <div class="row">
                    <div class="col-12">
                        <?= Html::submitButton('Update Password', ['class' => 'btn btn-primary btn-block']) ?>
                    </div>
                </div>

                <?php \yii\bootstrap4\ActiveForm::end(); ?>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
</div>