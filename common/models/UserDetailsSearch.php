<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserDetails;

/**
 * UserDetailsSearch represents the model behind the search form of `common\models\UserDetails`.
 */
class UserDetailsSearch extends UserDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role_id'], 'integer'],
            [['full_name', 'given_name', 'nic', 'emp_id', 'emp_st_date', 'birthday', 'gender', 'address', 'email', 'tel_no', 'mob_no', 'photo', 'emp_type', 'acc_from', 'acc_to'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'emp_st_date' => $this->emp_st_date,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'role_id' => $this->role_id,
            'acc_from' => $this->acc_from,
            'acc_to' => $this->acc_to,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'given_name', $this->given_name])
            ->andFilterWhere(['like', 'nic', $this->nic])
            ->andFilterWhere(['like', 'emp_id', $this->emp_id])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'tel_no', $this->tel_no])
            ->andFilterWhere(['like', 'mob_no', $this->mob_no])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'emp_type', $this->emp_type]);

        return $dataProvider;
    }
}
