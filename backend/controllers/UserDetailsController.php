<?php

namespace backend\controllers;

use Yii;
use common\models\UserDetails;
use common\models\UserDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserDetailsController implements the CRUD actions for UserDetails model.
 */
class UserDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['activation'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserDetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserDetails();

        // echo "<pre>";
        // var_dump(Yii::$app->request->post());die;
        if ($model->load(Yii::$app->request->post())) {

            $emp_st_date = strtotime(Yii::$app->request->post()['UserDetails']['emp_st_date']);
            $new_emp_st_date = date('Y-m-d',$emp_st_date);

            $birthday = strtotime(Yii::$app->request->post()['UserDetails']['birthday']);
            $new_birthday = date('Y-m-d',$birthday);

            $acc_from = strtotime(Yii::$app->request->post()['UserDetails']['acc_from']);
            $new_acc_from = date('Y-m-d',$acc_from);

            $acc_to = strtotime(Yii::$app->request->post()['UserDetails']['acc_to']);
            $new_acc_to = date('Y-m-d',$acc_to);


            $model->full_name = Yii::$app->request->post()['UserDetails']['full_name'];
            $model->given_name = Yii::$app->request->post()['UserDetails']['given_name'];
            $model->nic = Yii::$app->request->post()['UserDetails']['nic'];
            $model->emp_id = Yii::$app->request->post()['UserDetails']['emp_id'];
            $model->emp_st_date = $new_emp_st_date;
            $model->birthday = $new_birthday;
            $model->gender = Yii::$app->request->post()['UserDetails']['gender'];
            $model->address = Yii::$app->request->post()['UserDetails']['address'];
            $model->email = Yii::$app->request->post()['UserDetails']['email'];
            $model->tel_no = Yii::$app->request->post()['UserDetails']['tel_no'];
            $model->mob_no = Yii::$app->request->post()['UserDetails']['mob_no'];
            $model->photo = Yii::$app->request->post()['UserDetails']['photo'];
            $model->role_id = Yii::$app->request->post()['UserDetails']['role_id'];
            $model->emp_type = Yii::$app->request->post()['UserDetails']['emp_type'];
            $model->acc_from = $new_acc_from;
            $model->acc_to = $new_acc_to;
            $model->status = '1';
            $model->key = Yii::$app->security->generateRandomString();

            if($model->save()){
                $this->activation($model->id);
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function activation($Id){
        $user = UserDetails::findOne($Id);
        $url  = Yii::$app->urlManager->createAbsoluteUrl(['site/activate', 'key' => $user['key']]);

        $html = '<h3>Hi '.$user['given_name'].'</h3>';
        $html .= '<br>';
        $html .= '<p>Click the URL below to activate your ATL user account</p>';
        $html .= '<br>';
        $html .= '<b>Click this '.$url.'</b>';

        Yii::$app->mailer->compose()
            ->setFrom('info@atl-production.com')
            ->setTo('dinith26@gmail.com')
            ->setSubject('Welcome to the ATL')
            ->setTextBody($html)
            ->setHtmlBody($html)
            ->send();

        return true;
    }

    public function actionSend(){
        echo "<pre>";
        $user = UserDetails::findOne(1);
        var_dump($user['full_name']);die;
        $url  = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm', 'id' => '1']);
        $name = "Tharuka";
        $html = '<h3>Hi '.$name.'</h3>';
        $html .= '<br>';
        $html .= '<p>Click the URL below to activate your ATL user account</p>';
        $html .= '<br>';
        $html .= '<b>Click this '.$url.'</b>';

        Yii::$app->mailer->compose()
            ->setFrom('info@atl-production.com')
            ->setTo('dinith26@gmail.com')
            ->setSubject('Message subject')
            ->setTextBody('Plain text content')
            ->setHtmlBody($html)
            ->send();
    }

    /**
     * Finds the UserDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
