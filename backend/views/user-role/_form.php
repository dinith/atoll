<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserRole */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-role-form">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"><?= $title ?></h3>
		</div>
		<div class="card-body">
			<?php $form = ActiveForm::begin(); ?>
			<div class="row">
                <div class="col-sm-6">
                	<div class="form-group">
                		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                	</div>
                </div>
                <div class="col-sm-6">
                	<div class="form-group">
                		<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
                	</div>
                </div>
                <div class="col-sm-12">
                	<div class="form-group">
                		<?= $form->field($model, 'permission')->checkboxList($permissions, ['class' => "custom-checkbox"]); ?>
                	</div>
                </div>
            </div>
		    
		    <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		</div>
	</div>
    

</div>
