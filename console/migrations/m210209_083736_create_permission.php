<?php

use yii\db\Migration;

/**
 * Class m210209_083736_create_permission
 */
class m210209_083736_create_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
     public function up()
    {
        $this->createTable('permission', [
            'id' => $this->primaryKey(),
            'name' => $this->string(30)->notNull()->unique(),
            'description' => $this->string(50)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('permission');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210209_083736_create_permission cannot be reverted.\n";

        return false;
    }
    */
}
