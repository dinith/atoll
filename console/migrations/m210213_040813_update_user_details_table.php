<?php

use yii\db\Migration;

/**
 * Class m210213_040813_update_user_details_table
 */
class m210213_040813_update_user_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn('user_details', 'nic', $this->string(20)->notNull());
        $this->addColumn('user_details', 'status', $this->string(10));
        $this->addColumn('user_details', 'key', $this->text());
        $this->alterColumn('user_details', 'email', $this->string(50)->notNull()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m210212_164711_alter_gender_column_user_details_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210213_040813_update_user_details_table cannot be reverted.\n";

        return false;
    }
    */
}
