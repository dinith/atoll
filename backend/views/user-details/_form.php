<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\UserRole;


$roles = UserRole::find()->all();

$listData=ArrayHelper::map($roles,'id','name');
/* @var $this yii\web\View */
/* @var $model common\models\UserDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-form">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'given_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'nic')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'emp_id')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'emp_st_date')->widget(DatePicker::classname(), [
                            'dateFormat' => 'php:Y-m-d', 
                            'options' => [
                                'class' => 'form-control',
                                'readonly' => true
                            ], 
                            'clientOptions' => [ 
                                'changeMonth' => true, 
                                'changeYear' => true, 
                                'yearRange' => '1980:'.date('Y'), 
                                'maxDate' => 'd']
                            ]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                            'dateFormat' => 'php:Y-m-d', 
                            'options' => [
                                'class' => 'form-control',
                                'readonly' => true
                            ], 
                            'clientOptions' => [ 
                                'changeMonth' => true, 
                                'changeYear' => true, 
                                'yearRange' => '1960:'.date('Y'), 
                                'maxDate' => 'd']
                            ]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'gender')->dropDownList(['male' => 'Male', 'female' => 'Female'], ['prompt'=>'Select...']) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'tel_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'mob_no')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'photo')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'role_id')->dropDownList($listData,['prompt'=>'Select...']) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'emp_type')->dropDownList(['temporary' => 'Temporary', 'permanent' => 'Permanent'], ['prompt'=>'Select...']) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'acc_from')->widget(DatePicker::classname(), [
                            'dateFormat' => 'php:Y-m-d', 
                            'options' => [
                                'class' => 'form-control',
                                'readonly' => true
                            ], 
                            'clientOptions' => [ 
                                'changeMonth' => true, 
                                'changeYear' => true, 
                                'yearRange' => '1960:'.date('Y'), 
                                'maxDate' => 'd']
                            ]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'acc_to')->widget(DatePicker::classname(), [
                            'dateFormat' => 'php:Y-m-d', 
                            'options' => [
                                'class' => 'form-control',
                                'readonly' => true
                            ], 
                            'clientOptions' => [ 
                                'changeMonth' => true, 
                                'changeYear' => true, 
                                // 'yearRange' => '2020:'.date('Y'), 
                                'maxDate' => '+2048d']
                            ]) ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
