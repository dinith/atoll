<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "permission".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $module
 *
 * @property RolePermission[] $rolePermissions
 * @property UserRole $userRole
 */
class Permission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'module'], 'required'],
            [['name'], 'string', 'max' => 30],
            [['description'], 'string', 'max' => 50],
            [['module'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'module' => 'Module',
        ];
    }

    /**
     * Gets query for [[RolePermissions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRolePermissions()
    {
        return $this->hasMany(RolePermission::className(), ['permission_id' => 'id']);
    }

    /**
     * Gets query for [[UserRole]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'id']);
    }

    public static function getUserPermissionList(){
        $return = [];
        $list = static::find()->asArray()->all();
        foreach ($list as $key => $item) {
           $return[$item['id']] = strtoupper($item['module']) ." - ".$item['name'];
        }
        return $return;
    }

    public function getUserPermissionListById($Id){
        return static::findOne($Id);
    }
}
