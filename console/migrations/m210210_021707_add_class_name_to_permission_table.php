<?php

use yii\db\Migration;

/**
 * Class m210210_021707_add_class_name_to_permission_table
 */
class m210210_021707_add_class_name_to_permission_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('permission', 'module', $this->string(100)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('permission', 'module');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210210_021707_add_class_name_to_permission_table cannot be reverted.\n";

        return false;
    }
    */
}
