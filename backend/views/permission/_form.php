<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Permission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="permission-form">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
		    <?php $form = ActiveForm::begin(); ?>
		    <div class="row">
                <div class="col-6">
		    		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		    	</div>
		    	<div class="col-6">
		    		<?= $form->field($model, 'module')->textInput(['maxlength' => true]) ?>
		    	</div>
		    	<div class="col-12">
		    		<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
		    	</div>
		    </div>
		    <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		</div>
	</div>

</div>
