<?php

use yii\db\Migration;

/**
 * Class m210212_164711_alter_gender_column_user_details_table
 */
class m210212_164711_alter_gender_column_user_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn('user_details', 'gender', $this->string(10)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m210212_164711_alter_gender_column_user_details_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210212_164711_alter_gender_column_user_details_table cannot be reverted.\n";

        return false;
    }
    */
}
