<?php

use yii\db\Migration;

/**
 * Class m210210_163146_create_user_role
 */
class m210210_163146_create_user_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('user_role', [
            'id' => $this->primaryKey(),
            'name' => $this->string(30)->notNull()->unique(),
            'description' => $this->string(50)->notNull(),
            'permission' => $this->text()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('user_roles');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210210_163146_create_user_role cannot be reverted.\n";

        return false;
    }
    */
}
